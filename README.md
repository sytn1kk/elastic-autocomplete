# ElasticSearch Fuzzy
## ElasticSearch

## Run

- ```git clone https://gitlab.com/sytn1kk/elastic-autocomplete.git```
- ```cd elastic-autocomplete```
_ ```docker-compose -f "docker-compose.yml" up -d --build```
- Import dataset ```curl -XPUT localhost:9200/_bulk -H 'Content-Type: application/json' --data-binary @shakespeare_6.0.json```
- Make search request:
```curl --location --request GET 'http://localhost:9200/shakespeare/_search' --header 'Content-Type: application/json' --data-raw '{ "query": { "match": { "speaker": { "query": "AYTOLYCS", "fuzziness": "3" } } } }'```

## Screenshots

![Search result](https://i2.paste.pics/208a5021cebc6b30188f1b26330c313a.png "Search result")
